FROM nginx

RUN rm /bin/sh && ln -sf /bin/bash /bin/sh

RUN rm -Rf /usr/share/nginx/www

RUN rm /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx/

WORKDIR /app

COPY . /app

ENTRYPOINT ["nginx"]
